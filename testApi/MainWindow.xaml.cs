﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using testApi.WebClient;
using testApi.Entities;
using ServiceStack;
using testApi.Database;


namespace testApi
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private static Db _database;
    //instance databáze
    public static Db Database
    {
      get
      {
        if (_database == null)
        {
          var fileHelper = new FileHelper();
          _database = new Db(fileHelper.GetLocalFilePath("SQLite.db3"));
        }
        return _database;
      }
    }

    public Rest rest = new Rest();
    public MainWindow()
    {
      InitializeComponent();
      List<Todos> apiTodos = rest.getTodos();
      myList.ItemsSource = apiTodos.OrderBy(t => t.completed);
      foreach(Todos td in apiTodos)
      {
        saveTodos(td);
      }
    }

    public async void saveTodos(Todos td)
    {
      await Database.SaveTodosAsync(td);
    }
  }
}
