﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApi.Entities;
using ServiceStack;

namespace testApi.WebClient
{
  public class Rest
  {
    public IServiceClient client = new JsonServiceClient("http://jsonplaceholder.typicode.com/").WithCache();
    public List<Todos> getTodos()
    {
      try
      {
        string response = client.Get<string>("todos");
        return response.FromJson<List<Todos>>();
      }      
      catch(System.Net.WebException ex)
      {
        List<Todos> tdl = new List<Todos>();
        tdl.Add(new Todos { title = "error message: " + ex.Message });
        return tdl;
      }
    }
  }
}
