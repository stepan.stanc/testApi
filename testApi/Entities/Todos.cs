﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace testApi.Entities
{
  public class Todos
  {
    public int userId { get; set; }
    [PrimaryKey]
    public int id { get; set; }
    public string title { get; set; }
    public bool completed { get; set; }

    public override string ToString()
    {
      return title + " completed: " + completed.ToString();
    }
  }
  /*
   "userId": 6,
    "id": 120,
    "title": "dolorem laboriosam vel voluptas et aliquam quasi",
    "completed": false
   */
}
