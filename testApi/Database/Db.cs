﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApi.Entities;
using SQLite;

namespace testApi.Database
{
  public class Db
  {
    private SQLiteAsyncConnection db;

    public Db(string dbPath)
    {
      db = new SQLiteAsyncConnection(dbPath);
      db.CreateTableAsync<Todos>();
    }

    public Task<int> SaveTodosAsync(Todos item)
    {

      if (item.id != 0)
      {
        return db.UpdateAsync(item);
      }
      else
      {
        return db.InsertAsync(item);
      }
    }
  }
}
